#!/bin/sh
# Copyright (C) 2022 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

set -e

VERSION=$(cat VERSION)
FILENAME="libdnet-${VERSION}.tar.gz"
if [ "$VERSION" = "master" ]
 then URL="https://github.com/ofalk/libdnet/archive/refs/heads/master.tar.gz"
else
 URL="https://github.com/ofalk/libdnet/archive/refs/tags/libdnet-${VERSION}.tar.gz"
 VERSION="libdnet-$VERSION"
fi

rm -rf tmp
mkdir tmp
cd tmp

wget -nv -O "${FILENAME}" "${URL}"
tar xf "${FILENAME}"

cd ..
rsync -a "tmp/libdnet-${VERSION}/" libdnet/
