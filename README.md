# xivo-python-dumbnet-packaging

Packaging for python-dumbnet for debian in python3, based on the good work from ofalk's : https://github.com/ofalk/libdnet
(we use dnet as a dependency of xivo.solutions>xivo-sysconfd)

## Implementation  

+ ./fetch_tarball.sh retrieves the code from github and place it to a folder libdnet to not pollute root folder. It is called at build time.
+ debian/rules has been tweaked to do the relevant build parts within that folder
+ debian/install specifies the relevant file to keep
+ change VERSION to specify whether you want master or an earlier release (to check on the library repo). Tested for debian12 (python3.11) on master (1.18, February 2024)
